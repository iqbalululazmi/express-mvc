const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./routes/api');
const { checkConnection } = require('./libraries/database');

(async function () {
  await checkConnection();

  const app = express();

  const jsonParser = bodyParser.json();
  const urlParser = bodyParser.urlencoded({ extended: false });

  app.use(jsonParser);
  app.use(urlParser);

  //   console.log(routes())

  app.use('/api/v1', routes());

  // 404
  app.use(function (req, res, next) {
    res.status(404).send('Not Found');
  });

  // 500
  app.use(function (err, req, res, next) {
    if (err) {
      console.error(err.stack);
    }

    res.status(500).send('Something broke!');
  });

  app.listen(3000, function () {
    console.log('Server running on http://localhost:3000');
  });
})();
